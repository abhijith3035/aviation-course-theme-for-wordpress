<?php
/*
Template Name: Course Template
*/
get_header();
?>
<style>
    #hero {
        width: 100%;
        height: 50vh;
        background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/about.jpeg')top;
        background-size: cover;
        background-position: center;
        position: relative;
    }
</style>

<section id="hero">
    <div class="hero-container">
        <h3>Welcome to <strong>Aviatics</strong></h3>
        <h1>Courses</h1>
    </div>
</section><!-- End Hero -->

<section id="services" class="pricing">
    <div class="container">
        <div class="section-title">
            <h2>Course</h2>
            <h3>Elevating careers, <span>One flight at a time.</span></h3>
            <p>Our course offers competitive pricing without sacrificing quality, ensuring accessible education for aspiring aviation professionals.</p>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="box">
                    <h3>Diploma In Airport Management</h3>
                    <h4><sup>₹</sup>30000<span> /-</span></h4>
                    <ul>
                        <li>Duration 6 Months</li>
                        <li>Real-world simulations and internships</li>
                        <li>Personalized career guidance</li>
                        <li>Enroll Today and Take Off Towards Success!</li>
                    </ul>
                    <div class="btn-wrap">
                        <a href="#" class="btn-buy">Apply Now</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 mt-4 mt-md-0">
                <div class="box recommended">
                    <span class="recommended-badge">Offer</span>
                    <h3>PG Diploma In Airport Management</h3>
                    <h4><sup>₹</sup>45000<span> /-</span></h4>
                    <ul>
                        <li>Duration 1 year</li>
                        <li>Advanced curriculum</li>
                        <li>Personalized career guidance</li>
                        <li>Elevate Your Career in Aviation</li>
                    </ul>
                    <div class="btn-wrap">
                        <a href="#" class="btn-buy">Buy Now</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 mt-4 mt-lg-0">
                <div class="box">
                    <h3>Certificate Course In Airport Management</h3>
                    <h4><sup>₹</sup>15000<span> /-</span></h4>
                    <ul>
                        <li>Duration 1 Month</li>
                        <li>Practical training</li>
                        <li>Career development assistance</li>
                        <li>Secure Your Spot Today</li>
                    </ul>
                    <div class="btn-wrap">
                        <a href="#" class="btn-buy">Buy Now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- End Pricing Section -->


 <!-- ======= Features Section ======= -->
 <section id="features" class="features">
      <div class="container">

        <div class="row">
          <div class="col-lg-3 col-md-4 col-6 col-6">
            <div class="icon-box">
              <i class="ri-store-line" style="color: #ffbb2c;"></i>
              <h3><a href="">Comprehensive</a></h3>
            </div>
          </div>
          <div class="col-lg-3 col-md-4 col-6">
            <div class="icon-box">
              <i class="ri-bar-chart-box-line" style="color: #5578ff;"></i>
              <h3><a href="">Industry-focused</a></h3>
            </div>
          </div>
          <div class="col-lg-3 col-md-4 col-6 mt-4 mt-md-0">
            <div class="icon-box">
              <i class="ri-calendar-todo-line" style="color: #e80368;"></i>
              <h3><a href="">Expert-led</a></h3>
            </div>
          </div>
          <div class="col-lg-3 col-md-4 col-6 mt-4 mt-lg-0">
            <div class="icon-box">
              <i class="ri-paint-brush-line" style="color: #e361ff;"></i>
              <h3><a href="">Career-oriented</a></h3>
            </div>
          </div>
          <div class="col-lg-3 col-md-4 col-6 mt-4">
            <div class="icon-box">
              <i class="ri-database-2-line" style="color: #47aeff;"></i>
              <h3><a href="">Globally-recognized</a></h3>
            </div>
          </div>
          <div class="col-lg-3 col-md-4 col-6 mt-4">
            <div class="icon-box">
              <i class="ri-gradienter-line" style="color: #ffa76e;"></i>
              <h3><a href="">State-of-the-art</a></h3>
            </div>
          </div>
          <div class="col-lg-3 col-md-4 col-6 mt-4">
            <div class="icon-box">
              <i class="ri-file-list-3-line" style="color: #11dbcf;"></i>
              <h3><a href="">Innovative</a></h3>
            </div>
          </div>
          <div class="col-lg-3 col-md-4 col-6 mt-4">
            <div class="icon-box">
              <i class="ri-price-tag-2-line" style="color: #4233ff;"></i>
              <h3><a href="">Dynamic</a></h3>
            </div>
          </div>
          <div class="col-lg-3 col-md-4 col-6 mt-4">
            <div class="icon-box">
              <i class="ri-anchor-line" style="color: #b2904f;"></i>
              <h3><a href="">Cutting-edge</a></h3>
            </div>
          </div>
          <div class="col-lg-3 col-md-4 col-6 mt-4">
            <div class="icon-box">
              <i class="ri-disc-line" style="color: #b20969;"></i>
              <h3><a href="">Tailored</a></h3>
            </div>
          </div>
          <div class="col-lg-3 col-md-4 col-6 mt-4">
            <div class="icon-box">
              <i class="ri-base-station-line" style="color: #ff5828;"></i>
              <h3><a href="">Career-driven</a></h3>
            </div>
          </div>
          <div class="col-lg-3 col-md-4 col-6 mt-4">
            <div class="icon-box">
              <i class="ri-fingerprint-line" style="color: #29cc61;"></i>
              <h3><a href="">Multidisciplinary</a></h3>
            </div>
          </div>
        </div>

      </div>
    </section><!-- End Features Section -->






<!-- ======= Cta Section ======= -->
<section id="cta" class="cta">
      <div class="container">

        <div class="text-center">
          <h3>Call To Action</h3>
          <p>
          Ready to take flight towards your dream career in aviation? Enroll in our courses today and let us
           guide you to success in the dynamic world of aviation management. Don't miss out on this opportunity 
           to soar to new heights – join us now!
          </p>
          <a class="cta-btn" href="tel:+91 9958873874">Call To Action</a> <a class="cta-btn" href="https://api.whatsapp.com/send?phone=9656227714">Contact via WhatsApp</a>
        </div>

      </div>
    </section><!-- End Cta Section -->




<?php get_footer(); ?>
