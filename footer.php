<footer id="footer">

    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-contact">
          <a href="<?php echo esc_url(home_url('/')); ?>" class="logo"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/logo.svg" alt="" class="img-fluid" height=50% width=50%></a>
            <p>
            2nd floor Kaloor Complex <br>
            Kaloor<br>
            Kochi,
 Kerala 682017 <br><br>
              <strong>Phone:</strong> <a href="tel:+919958873874"> +919958873874</a><br>
              <strong>Email:</strong><a href="mailto:training@smeclabs.org"> training@smeclabs.org</a><br>
            </p>
          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Useful Links</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="<?php echo esc_url(home_url('/')); ?>">Home</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="<?php echo esc_url(home_url('/index.php//about')); ?>">About us</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="<?php echo esc_url(home_url('/index.php//course')); ?>">Courses</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="<?php echo esc_url(home_url('/index.php//contact')); ?>">Connect</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="<?php echo esc_url(home_url('/index.php//privacy')); ?>">Privacy policy</a></li>
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Our Courses</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Diploma In Airport Management</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">PG Diploma In Airport Management</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Certificate Course In Airport Management</a></li>
              <!--<li><i class="bx bx-chevron-right"></i> <a href="#">Marketing</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Graphic Design</a></li>-->
            </ul>
          </div>

          <div class="col-lg-4 col-md-6 footer-newsletter">
            <h4>Join Our Newsletter</h4>
            <p>Stay informed, stay connected - Join our newsletter for the latest updates and insights in aviation management!</p>
            <form action="" method="post">
              <input type="email" name="email"><input type="submit" value="Subscribe">
            </form>
          </div>

        </div>
      </div>
    </div>

    <div class="container d-md-flex py-4">

      <div class="me-md-auto text-center text-md-start">
        <div class="copyright">
          &copy; Copyright <strong><span>Tempo</span></strong>. All Rights Reserved
        </div>
        <div class="credits">
        
          Designed by <a href="https://www.linkedin.com/in/abhijith-ps-118619142/">Abhijith</a>
        </div>
      </div>
      <div class="social-links text-center text-md-right pt-3 pt-md-0">
        <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
        <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
        <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
        <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
        <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
      </div>
    </div>
</footer><!-- End Footer -->

<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

<!-- Vendor JS Files -->
<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/vendor/glightbox/js/glightbox.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/vendor/swiper/swiper-bundle.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/vendor/php-email-form/validate.js"></script>

<!-- Template Main JS File -->
<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/main.js"></script>

</body>

</html>
